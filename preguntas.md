¿Quiénes son las personas que reciben los reportes?
- Operadores del 911 a nivel nacional, de ahí se remite al C5 correspondiente (asignados por zonas/estados)
¿Qué capacitación tienen?
¿Dónde vacían la información?
- Se vacían directamente en la herramienta que utiliza el 911 a nivel nacional
¿Por qué no hay datos antes de 2017?
¿Existen datos que no estén digitalizados?
¿Qué preprocesamiento se realizó antes de publicar los datos?
- Se remueven datos de los operadores y de las unidades por tema de datos personales
¿Las corporaciones llenan algún dato?
- La unidad no llena ninguna información, sino que la llena el C4, quienes son encargadxs de asignar una unidad

Específicas sobre la información en la base de datos
- Clarificar los valores de los atributos:
  - corporación
  - descripción_cierre
    - Dudas pendientes
  - vía_recepción
    - dudas pendientes
¿Es posible obtener el tiempo de duración de la llamada?
¿Es posible obtener el tiempo de llegada al punto de reporte?


