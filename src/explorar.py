import numpy as np
import pandas as pd
import utilerias as ut


class BaseDeDatos:
    """Clase que manipula los datos trabajados."""

    def __init__(self):
        """Función que inicializa la clase.
        Se establecen los métodos que se deben ejecutar al inicializar la clase.
        """

        self.leer_datos()
        self.definir_variables()
        self.preprocesar_datos()
        self.visualizar_basicos()
        self.imprimir_atributos_agrupados()

    def leer_datos(self):
        """Método que lee los datos."""

        data_file = "data/dvgm.csv"
        data_path = ut.abs_path(data_file)
        self.df = pd.read_csv(data_path)

    def definir_variables(self):
        """Método que define variables necesarias respecto a los datos."""

        self.atributos = self.df.columns.values

    def preprocesar_datos(self):
        """Método que preprocesa los datos que lo requieren."""
        self.df.loc[:, "fecha_completa"] = self.df.loc[:, "fecha"] + \
            " " + self.df.loc[:, "hora_recepcion"]

        self.df.loc[:, "fecha_completa"] = pd.to_datetime(
            self.df.loc[:, "fecha_completa"], format="%d/%m/%y %H:%M:%S")

    def visualizar_basicos(self):
        """Método que imprime una visualización básica de los datos."""

        print("Visualización de la base de datos")
        print(self.df)

        print("\nVisualización de los atributos")
        print(self.atributos)

        print("\nVisualización de los tipos de datos")
        print(self.df.info())

    def imprimir_atributos_agrupados(self):
        """Método que imprime los valores de cada atributo y su respectivo conteo."""

        for atributo in self.atributos:
            recopilado = self.df.groupby(atributo).count().iloc[:, 0].copy()
            print("\n\nAtributo agrupado: {}\n".format(atributo))
            print(recopilado)


if __name__ == "__main__":
    bd = BaseDeDatos()
