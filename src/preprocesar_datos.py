import numpy as np
import pandas as pd
import utilerias as ut


def reformatea_fecha(data):
    """Corrige el error en el formato de la fecha."""
    data.loc[:, "fecha"] = pd.to_datetime(data.loc[:, "fecha"], format="%d/%m/%y")

    data_date_wrong = data.loc[(data.fecha.dt.day >= 1) & (data.fecha.dt.day <= 12)]
    data_date_right = data.loc[data.fecha.dt.day >= 13]

    data_date_wrong.loc[:, "fecha"] = data_date_wrong.loc[:, "fecha"].dt.date
    data_date_right.loc[:, "fecha"] = data_date_right.loc[:, "fecha"].dt.date

    data_date_wrong.loc[:, "fecha"] = pd.to_datetime(
        data_date_wrong.loc[:, "fecha"], format="%Y-%d-%m"
    )
    data_date_right.loc[:, "fecha"] = pd.to_datetime(
        data_date_right.loc[:, "fecha"], format="%Y-%m-%d"
    )

    new_data = pd.concat([data_date_wrong, data_date_right])
    new_data.loc[:, "fecha"] = new_data.loc[:, "fecha"].dt.strftime(
        date_format="%Y-%m-%d"
    )
    return new_data


def formatea_fecha(data):
    """Formatea para tener fecha completa con hora, minutos y segundos."""
    data.loc[:, "fecha_completa"] = (
        data.loc[:, "fecha"] + " " + data.loc[:, "hora_recepcion"]
    )

    data.loc[:, "fecha_completa"] = pd.to_datetime(
        data.loc[:, "fecha_completa"], format="%Y-%m-%d %H:%M:%S"
    )


def limpia_descripcion_cierre(data):
    renombre_de_valores = {
        "Cierre De Fuga": "Otros",
        "Fuera De Entidad Federativa": "Otros",
        "Fuera De Subzona": "Otros",
        "Remitido Al M.p.": "Hecho real",
        "Resolucion 1": "Hecho real",
        "Resolucion 5": "Hecho real",
    }
    data.loc[:, "descripcion_cierre"] = data.loc[:, "descripcion_cierre"].replace(
        renombre_de_valores
    )


def limpia_via_recepcion(data):
    renombre_de_valores = {
        "060 Monterrey": "Otros",
        "80": "Otros",
        "89": "Otros",
        "App": "Otros",
        "App 9-1-1": "Otros",
        "Cic": "Otros",
        "Rutina": "Gobierno",
        "Sala crisis": "Gobierno",
        "Telefonia movil": "Telefono",
        "Videovigilancia": "Gobierno",
        "Whats App": "Otros",
    }
    data.loc[:, "via_recepcion"] = data.loc[:, "via_recepcion"].replace(
        renombre_de_valores
    )


def guarda_en_csv(data):
    nombre_de_archivo = "data/preprocessed_dvgm.csv"
    data_path = ut.abs_path(nombre_de_archivo)
    data.to_csv(data_path)


def run(data):
    reformatted_data = reformatea_fecha(data)
    formatea_fecha(reformatted_data)
    limpia_descripcion_cierre(reformatted_data)
    limpia_via_recepcion(reformatted_data)
    guarda_en_csv(reformatted_data)
    return reformatted_data
