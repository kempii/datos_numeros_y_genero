import os


def abs_path(filename):
    """Obtiene el camino absoluto a un archivo."""
    file_path = os.path.abspath(
        os.path.join(
            os.path.join(os.path.dirname(__file__), ".."),
            filename,
        )
    )
    return file_path
